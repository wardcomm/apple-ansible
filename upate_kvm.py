#!/usr/bin/python

import sys
import cloudstackapi, cloudstackjob, json, socket
import paramiko
import string
import os
import time
import select
import logging
import getpass
import re
from fabric.api import *

CHOICES = {     0:"https://monsoon-dev-cs.corp.apple.com/client/api",
                1:"https://monsoon-uat-cs.corp.apple.com/client/api",
                2:"http://rn2-monsoon-np-lcs.rno.apple.com:8080/client/api",
		3:"https://monsoon-prod-cs-ma1.corp.apple.com/client/api",
                4:"https://monsoon-prod-cs-nwk.corp.apple.com/client/api",
                5:"http://monsoon-prod-cs-rno.rno.apple.com:8080/client/api",
                6:"http://pr3-monsoonp-lapp01.prz.apple.com:8080/client/api",
          }

LOCATION = 0
CSURL = ''

try:
	print
	print("0. Maiden_Dev_Prod")
	print("1. Maiden_Test_Prod")              
	print("2. Reno_NonProd")
	print("3. Maiden_Prod")       
	print("4. Newark_Prod")      
	print("5. Reno_Prod")
	print("6. Prineville_Prod")
	print
	LOCATION = int(input("Select a CloudStack Environment: "))
	if (LOCATION >= 0) and (LOCATION <= 8):
		CSURL = CHOICES[LOCATION]
	else:
		print
		print("Please enter between 0 to 8, exiting....")
		quit()
except (NameError, ValueError, SyntaxError, KeyboardInterrupt):
	print
	print("Please enter between 0 to 8, exiting....")
	quit()

print
CSUSER = raw_input("Enter Your Cloudstack USERID: ") 
CSPASS = getpass.getpass("Enter Your Cloudstack Passwd: ") 
print
SSHUSER = raw_input("Enter Your SSH USERID: ") 
SSHPASS = getpass.getpass("Enter Your SSH Passwd: ")
print
BUILDNO = raw_input("Enter the Build Number to upgrade: ") 
HOSTS = []
HOSTSID = []
BUILD = []
UPREQ = []
NOVMS = []
SSHABLE = []
CSHOSTSTATE = []
CSRESSTATE = []
HOSTDB = {}
TARGETVER = "oVirt Node Hypervisor 3.5 (0.999."+BUILDNO+".el7)"
print "Target Version is ",TARGETVER

try:
	csapi = cloudstackapi.cloudstackapi({"serverurl":CSURL, "username":CSUSER, "password":CSPASS})
except:
	print "Error connecting to Cloudstack, check your credentials or VPN connection and retry"


def list_zones():
	try:
		ZONES = csapi.callApi("listZones");
		print
        	print ("List of Zones in this Cloudstack....");
        	print 
		print ("{:40}{:20}".format("Zone ID","ZONE Name"))
		print ( "-" * 80 )
		for zone in ZONES["zone"]:
			print ( "{:40}{:20}".format( zone["id"],zone["name"] ) )

	except:
		print
		print "Error connecting to Cloudstack, check your credentials or VPN connection and retry"
		quit()

def list_pods(ZONEID):
	try:
		PODS = csapi.callApi("listPods",{"zoneid":ZONEID})
		print
		print ("List of PODs in this Zone....")
		print
		print ("{:40}{:20}".format("POD ID","POD Name"))
		print ( "-" * 80 )
		for pod in PODS["pod"]:
			print ( "{:40}{:20}".format( pod["id"],pod["name"] ) )
	except(NameError, ValueError, SyntaxError, KeyboardInterrupt, KeyError):
		print		
		print("Oops! Not a valid Zone id... exiting...")
		quit()

def list_clusters(PODID):
        try:
		CLUSTERS = csapi.callApi("listClusters",{"podid":PODID});
		print
        	print ("List of KVM Clusters in this POD....")
        	print
        	print ("{:40}{:45}{:20}".format("Cluster ID","Cluster Name","Cluster Type" ) )
		print ( "-" * 105 )
		for cluster in CLUSTERS["cluster"]:
			if cluster["hypervisortype"] == "KVM":
                		print ( "{:40}{:45}{:20}".format(cluster["id"],cluster["name"],cluster["hypervisortype"] ) )
			else:
				print
				print "No KVM Clusters in this POD... exiting"
				quit()
	except(NameError, ValueError, SyntaxError, KeyboardInterrupt):
                print
                print("Oops! Not a valid POD id... exiting...")
                exit(1)

def find_hosts(CLUSTERID):
	try:
	        ALLHOSTS = csapi.callApi("listHosts",{"clusterid":CLUSTERID});
        	for host in ALLHOSTS["host"]:
			if host["hypervisor"] == "KVM":
				HOSTS.append(host["name"])
				HOSTSID.append(host["id"])
				CSHOSTSTATE.append(host["state"])
				CSRESSTATE.append(host["resourcestate"])
			else:
				print
				print "Please select only KVM host's POD's or Cluster to upgrade... exiting"
				quit()
	except(NameError, ValueError, SyntaxError, KeyboardInterrupt):
                print
                print("Oops! Not a valid Cluster id... exiting...")
                exit(1)

def list_vms(HOSTID):
        try:
                ALLVMS = csapi.callApi("listVirtualMachines",{"listAll":"true","hostid":HOSTID});
		print
                print ("List of VM's running on this HOST....")
                print 
                print ("{:40}{:30}{:40}{:30}{:30}{:30}".format("VM ID","VM Name","DISPLAY Name","Instance Name","Template Name","ServiceOffering Name"))
                print ("-" * 200)
                for vm in ALLVMS["virtualmachine"]:
                        print ( "{:40}{:30}{:40}{:30}{:30}{:30}".format(vm["id"],vm["name"],vm["displayname"],vm["instancename"],vm["templatename"],vm["serviceofferingname"]) )
        except(NameError, ValueError, SyntaxError, KeyboardInterrupt):
                print
                print("Oops! Not a valid Host id... exiting...")
                exit(1)

def find_sshable(HOSTS):
	HOST=0
        while HOST<len(HOSTS):
    		try:
			ssh = paramiko.SSHClient()
    			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(HOSTS[HOST], username=SSHUSER, password=SSHPASS)
    			output = "Yes"	
			SSHABLE.append(output)
#			print(output)
			paramiko.util.log_to_file("ssh.log")
		except paramiko.AuthenticationException:
			output = "Auth Err"
                        SSHABLE.append(output)
#			print(output)
			paramiko.util.log_to_file("ssh.log")
		except:
			output = "No"
                        SSHABLE.append(output)
#			print(output)
			paramiko.util.log_to_file("ssh.log")
		ssh.close()
                HOST+=1
		
def find_build(HOSTS):
        HOST=0
        while HOST<len(HOSTS):
                try:
                        ssh = paramiko.SSHClient()
                        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                        ssh.connect(HOSTS[HOST], username=SSHUSER, password=SSHPASS)	
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("cat /etc/ovirt-node-iso-release")
			output = ssh_stdout.read()
			output = output.rstrip()
        		BUILD.append(output)
			paramiko.util.log_to_file("ssh.log")
		except:
			output = "Auth Err or Host Down, check it manually"
			BUILD.append(output)
			paramiko.util.log_to_file("ssh.log")
    		ssh.close()				
		if output == TARGETVER:
                     	upreq = "No"
              	elif output == "Auth Err or Host Down, check it manually":
			upreq = "n/a"
		else:
                   	upreq = "Yes"
                UPREQ.append(upreq)
		HOST+=1

def find_novms(HOSTSID):
        HOST=0
        while HOST<len(HOSTSID):
		try:
			VMS = csapi.callApi("listVirtualMachines",{"listAll":"true","hostid":HOSTSID[HOST]})
			NOVMS.append(VMS["count"])
#			print (HOSTSID[HOST])	
#			print (VMS["count"])
                except:
			NOVMS.append(0)
		HOST+=1

def list_hostdb(CLUSTERID,HOSTS,HOSTSID,BUILD,NOVMS,SSHABLE,CSHOSTSTATE,CSRESSTATE,UPREQ):
        HOSTUP=0
	HOSTDOWN=0
	HOSTALERT=0
	HOSTERROR=0
	HOSTMAINT=0
	HOSTDC=0
	HOSTCONN=0
	HOSTPREP=0
	HOST=0
	print
        print ("{:40}{:45}{:20}{:20}{:20}{:20}{:20}".format("Host Name","oVirt Version","No. of Running VMs","SSHable?","CS Host Status","CS Resource State", "Update Required?"))
        print ("-" * 185)

        while HOST<len(HOSTS):
		HOSTDB[HOSTS[HOST]] = { 'hostid':HOSTSID[HOST], 'build':BUILD[HOST], 'novms':NOVMS[HOST], 'sshable':SSHABLE[HOST], 'cshoststate':CSHOSTSTATE[HOST], 'csresstate':CSRESSTATE[HOST], 'upreq':UPREQ[HOST] }
		HOST+=1

	for host in HOSTDB:
		print ("{:40}{:45}{:20}{:20}{:20}{:20}{:20}".format(host, HOSTDB[host]['build'], str(HOSTDB[host]['novms']), HOSTDB[host]['sshable'], HOSTDB[host]['cshoststate'], HOSTDB[host]['csresstate'], HOSTDB[host]['upreq']))
		
		if HOSTDB[host]['sshable'] == "Yes":
			if HOSTDB[host]['cshoststate'] == "Up":
                        	if HOSTDB[host]['csresstate'] == "Enabled":
					HOSTUP+=1
				elif HOSTDB[host]['csresstate'] == "PrepareForMaintenance":
                                	HOSTPREP+=1
                        	elif HOSTDB[host]['csresstate'] == "ErrorInMaintenance":
                                	HOSTERROR+=1
                        	elif HOSTDB[host]['csresstate'] == "Maintenance":
					HOSTMAINT+=1
			elif HOSTDB[host]['cshoststate'] == "Alert":
        	                HOSTALERT+=1
			elif HOSTDB[host]['cshoststate'] == "Connecting":
                                HOSTCONN+=1
                        elif HOSTDB[host]['cshoststate'] == "Disconnected":
                                HOSTDC+=1
		elif HOSTDB[host]['sshable'] == "No":
			if HOSTDB[host]['cshoststate'] == "Down":
				HOSTDOWN+=1	
			elif HOSTDB[host]['cshoststate'] == "Disconnected":
				HOSTDC+=1
		elif HOSTDB[host]['sshable'] == "Auth Err":
			HOSTERROR+=1
			
	print
	print "Total No. of Hosts in this Cluster: ", len(HOSTDB)
	print "No. of HOSTS Up: ", HOSTUP
        print "No. of HOSTS Down: ", HOSTDOWN
	print
	if HOSTALERT > 0:	print "No. of HOSTS in Alert State: ", HOSTALERT
	if HOSTPREP > 0:	print "No. of HOSTS in Prepare for Maint. State: ", HOSTPREP
	if HOSTERROR > 0:	print "No. of HOSTS in Error State: ", HOSTERROR
	if HOSTMAINT > 0:	print "No. of HOSTS in Maintenance State: ", HOSTMAINT
	if HOSTDC > 0:		print "No. of HOSTS in Disconnected State: ", HOSTDC
	if HOSTCONN > 0:	print "No. of HOSTS in Connecting State: ", HOSTCONN

	list_cap(CLUSTERID)

	if (HOSTALERT > 1) or (HOSTERROR > 1) or (HOSTDC > 1):
		print
		print "Some of the HOSTS are in Alert/Error/Disconnected State, Please fix those hosts before upgrading to create space for migrating vm's.. exiting..."
		quit()

def list_cap(CLUSTERID):
	print
	print "Listing the current capacity in the cluster"
	print
	print ("{:30}{:30}{:30}{:30}{:30}".format("Resource Type","Total Capacity","Available Capacity","Used Capacity","Used Percentage"))
	print ("-" * 150)

        try:
                CAPCPU = csapi.callApi("listCapacity",{"clusterid":CLUSTERID,"type":"1"})
                for cap in CAPCPU["capacity"]:
			capacityavail=(cap["capacitytotal"]-cap["capacityused"])
                        print ("{:30}{:30}{:30}{:30}{:30}".format("CPU: ",str(cap["capacitytotal"])+" MHz",str(capacityavail)+" MHz",str(cap["capacityused"])+" MHz",str(cap["percentused"]+"%")))
        except(NameError, ValueError, SyntaxError, KeyboardInterrupt):
                print
                print("Oops! Not a valid Cluster id... exiting...")
                exit(1)
	except(KeyError):
                print ("{:30}{:30}".format("CPU: ","Value not exist in Cloudstack, please check the CS settings"))
	
        try:
                CAPMEM = csapi.callApi("listCapacity",{"clusterid":CLUSTERID,"type":"0"})
	   	for cap in CAPMEM["capacity"]:
			capacityavail=(cap["capacitytotal"]-cap["capacityused"])
			print ("{:30}{:30}{:30}{:30}{:30}".format("MEMORY: ",str(cap["capacitytotal"]/1024**3)+" GB",str(capacityavail/1024**3)+" GB",str(cap["capacityused"]/1024**3)+" GB",str(cap["percentused"]+"%")))
        except(NameError, ValueError, SyntaxError, KeyboardInterrupt):
                print
                print("Oops! Not a valid Cluster id... exiting...")
                exit(1)
	except(KeyError):
                print ("{:30}{:30}".format("MEMORY: ","Value not exist in Cloudstack, please check the CS settings"))
	
	try:
                CAPSTORAGE = csapi.callApi("listCapacity",{"clusterid":CLUSTERID,"type":"2"})
                for cap in CAPSTORAGE["capacity"]:
			capacityavail=(cap["capacitytotal"]-cap["capacityused"])
                        print ("{:30}{:30}{:30}{:30}{:30}".format("ACTUAL STORAGE: ",str(cap["capacitytotal"]/1024**3)+" GB",str(capacityavail/1024**3)+" GB",str(cap["capacityused"]/1024**3)+" GB",str(cap["percentused"]+"%")))
        except(NameError, ValueError, SyntaxError, KeyboardInterrupt):
                print
                print("Oops! Not a valid Cluster id... exiting...")
                exit(1)
	except(KeyError):
                print ("{:30}{:30}".format("ACTUAL STORAGE: ","Value not exist in Cloudstack, please check the CS settings"))

        try:
                CAPSTORAGEALLOC = csapi.callApi("listCapacity",{"clusterid":CLUSTERID,"type":"3"})
                for cap in CAPSTORAGEALLOC["capacity"]:
			capacityavail=(cap["capacitytotal"]-cap["capacityused"])
                        print ("{:30}{:30}{:30}{:30}{:30}".format("ALLOCATED STORAGE: ",str(cap["capacitytotal"]/1024**3)+" GB",str(capacityavail/1024**3)+" GB",str(cap["capacityused"]/1024**3)+" GB",str(cap["percentused"]+"%")))
        except(NameError, ValueError, SyntaxError, KeyboardInterrupt):
                print
                print("Oops! Not a valid Cluster id... exiting...")
                exit(1)
	except(KeyError):
		print ("{:30}{:30}".format("ALLOCATED STORAGE: ","Value not exist in Cloudstack, please check the CS settings"))

def update_host(HOSTDB):
	print
	HOSTUPDATE = raw_input("Enter the Hostname to update: ")
	for host in HOSTDB:
		if (host == HOSTUPDATE):
           		if (HOSTDB[host]['upreq'] == "Yes"):
				if (HOSTDB[host]['cshoststate'] == "Up"):
					if (HOSTDB[host]['csresstate'] == "Enabled"):
                				if HOSTDB[host]['novms'] > 0:
							HOSTUPDATEID=HOSTDB[host]['hostid']
							list_vms(HOSTUPDATEID)
	                                                print
        	                                        PREF = raw_input("Do you prefer any specific host to migrate the VMs (y/n): ")
                                                        if PREF == 'y' or PREF == 'Y':
								print
                                                                PREFHOST = raw_input("Enter the Preferred Hostname: ")
								PREFHOSTID=HOSTDB[PREFHOST]['hostid']
                                                                migrate_vms(HOSTUPDATE,HOSTUPDATEID,PREFHOST,PREFHOSTID)
								prep_maint(HOSTUPDATE,HOSTUPDATEID)
								check_maint(HOSTUPDATE,HOSTUPDATEID)
								host_update(HOSTUPDATE,HOSTUPDATEID)
								check_hostback(HOSTUPDATE,HOSTUPDATEID)
								check_build(HOSTUPDATE,HOSTUPDATEID)
#								cancel_maint(HOSTUPDATE,HOSTUPDATEID)
								post_state(HOSTUPDATE,HOSTUPDATEID)
								break
                                                        elif PREF == 'n' or PREF == 'N':
                                                                print
                                                                print "continuing random pick host for migration..."
								prep_maint(HOSTUPDATE,HOSTUPDATEID)
								check_maint(HOSTUPDATE,HOSTUPDATEID)
                                                                host_update(HOSTUPDATE,HOSTUPDATEID)
                                                                check_hostback(HOSTUPDATE,HOSTUPDATEID)
                                                                check_build(HOSTUPDATE,HOSTUPDATEID)
#                                                               cancel_maint(HOSTUPDATE,HOSTUPDATEID)
                                                                post_state(HOSTUPDATE,HOSTUPDATEID)
								break
                                                        else:
                                                                print
                                                                print "Not a valid key, exiting..."
								quit()
						else:
							HOSTUPDATEID=HOSTDB[host]['hostid']
							print "No Running VM's on this host...proceeding..."	
							prep_maint(HOSTUPDATE,HOSTUPDATEID)
							check_maint(HOSTUPDATE,HOSTUPDATEID)
                                                        host_update(HOSTUPDATE,HOSTUPDATEID)
                                                        check_hostback(HOSTUPDATE,HOSTUPDATEID)
                                                        check_build(HOSTUPDATE,HOSTUPDATEID)
#                                                       cancel_maint(HOSTUPDATE,HOSTUPDATEID)
                                                        post_state(HOSTUPDATE,HOSTUPDATEID)
							break
					elif (HOSTDB[host]['csresstate'] == "PrepareForMaintenance"):
						print
						print "Host is Preparing for Maintenance... continuing"
						HOSTUPDATEID=HOSTDB[host]['hostid']
						check_maint(HOSTUPDATE,HOSTUPDATEID)
						host_update(HOSTUPDATE,HOSTUPDATEID)
                                                check_hostback(HOSTUPDATE,HOSTUPDATEID)
                                                check_build(HOSTUPDATE,HOSTUPDATEID)
#                                               cancel_maint(HOSTUPDATE,HOSTUPDATEID)
                                                post_state(HOSTUPDATE,HOSTUPDATEID)
						break
					elif (HOSTDB[host]['csresstate'] == "Maintenance"):
						print
						print "Host is already in Maintenance state, proceeding upgrade"
						HOSTUPDATEID=HOSTDB[host]['hostid']
						host_update(HOSTUPDATE,HOSTUPDATEID)
						check_hostback(HOSTUPDATE,HOSTUPDATEID)
						check_build(HOSTUPDATE,HOSTUPDATEID)
#						cancel_maint(HOSTUPDATE,HOSTUPDATEID)
						post_state(HOSTUPDATE,HOSTUPDATEID)
						break
					elif (HOSTDB[host]['csresstate'] == "ErrorInMaintenance"):
                                                print
                                                print "Host is in Error State, Can't proceed upgrade... exiting..."
						quit()
                        	elif (HOSTDB[host]['cshoststate'] == "Alert" or HOSTDB[host]['cshoststate'] == "Down" or HOSTDB[host]['cshoststate'] == "Disconnected" or HOSTDB[host]['cshoststate'] == "Connecting"):
                                	print
					print "Host is in ALERT/Down/Disconnected/Connecting State, can't proceed update, please fix the host manually and retry...."
					quit()
              		elif (HOSTDB[host]['upreq'] == "No"):
                     		print HOSTUPDATE+" is already running required verison, no update required... exiting"
				break
			elif (HOSTDB[host]['upreq'] == "n/a"):
				print HOSTUPDATE+" is a down host, please fix the host first and retry updating OS... exiting"
				break
	else:
		print HOSTUPDATE+" is not part of this cluster..."
	continue_host()

def migrate_vms(HOSTUPDATE,HOSTUPDATEID,PREFHOST,PREFHOSTID):
	print
	print "Migrating vm's to preferred host...."
	try:
                ALLVMS = csapi.callApi("listVirtualMachines",{"listAll":"true","hostid":HOSTUPDATEID});
        except(NameError, ValueError, SyntaxError, KeyboardInterrupt):
                print
                print("Oops! Not a valid Host id... exiting...")
                exit(1)
	queue = cloudstackapi.asyncjobqueue(4, True, 5);
	if "virtualmachine" in ALLVMS:
		for vm in ALLVMS["virtualmachine"]:
                	queue.append(cloudstackjob.cloudstackjob("migrateVirtualMachine",{"virtualmachineid":vm["id"],"hostid":PREFHOSTID}))
			print "migrating "+vm["name"]+" from "+HOSTUPDATE+" to "+PREFHOST

	if queue.hasjobs():
    		queue.run(csapi)
    		for y in queue.processed:
    	   		if y.succeeded():
      				print y.result
      			else:
   				print "a job failed: %s" % y.result


def prep_maint(HOSTUPDATE,HOSTUPDATEID):
	print
	print HOSTUPDATE +" is preparing for Maintenance....Pls wait..."
	try:
		response = csapi.callApi("prepareHostForMaintenance",{"id":HOSTUPDATEID})
		result = csapi.watchJob(response["jobid"])
		if result["jobresult"]["host"]["resourcestate"] == "PrepareForMaintenance":
			print
			print HOSTUPDATE +" is in prepare for Maintenance state now..."
	except Exception as e:
		print "error: %s" % e
		print HOSTUPDATE+" is having trouble Preparing Host for  Maintenance, Please check the host manually... exiting..."
                quit()

def check_maint(HOSTUPDATE,HOSTUPDATEID):
	print
	print "Checking "+HOSTUPDATE+" is in maintenance or not....Pls wait..."
	try:
		response = csapi.callApi("listHosts",{"id":HOSTUPDATEID})
		for host in response["host"]:
			HOST_STATUS = host["resourcestate"]
		print HOST_STATUS
	except Exception as e:
                print "error: %s" % e
                print HOSTUPDATE+" is having Error in  Maintenance, Please check the host manually... exiting..."
                quit()
      	
	counter=0 
        while HOST_STATUS != "Maintenance":
             	print HOSTUPDATE+" is still in the Prepare for Maintenance state...pls wait"
		try:
                        VMS = csapi.callApi("listVirtualMachines",{"listAll":"true","hostid":HOSTUPDATEID})
                        NOVMS=(VMS["count"])
                except:
                        NOVMS=0
		print
		print "Current count of running vm's: ",NOVMS
       		counter+=1
       		time.sleep(30)
		response = csapi.callApi("listHosts",{"id":HOSTUPDATEID})
            	for host in response["host"]:
                        HOST_STATUS = host["resourcestate"]
		if HOST_STATUS == "ErrorInMaintenance":
			print
                	print HOSTUPDATE+" is having Error in  Maintenance, Please check the host manually... exiting..."
                	quit()
		if counter >= 60:
			print
			print HOSTUPDATE+" is taking long to enter into  Maintenance, May be something wrong on the host... Please check the host manually... Cancelling Maintenance & exiting..."
                        cancel_maint(HOSTUPDATE,HOSTUPDATEID)
			quit()	

	if HOST_STATUS == "Maintenance":
		try:
        		VMS = csapi.callApi("listVirtualMachines",{"listAll":"true","hostid":HOSTUPDATEID})
        		NOVMS=(VMS["count"])
              	except:
			NOVMS=0
	
		if NOVMS > 0:
			print
                	print HOSTUPDATE+" is still having some running VM's, It cannot be upgraded.. Please migrate the hosts manually and retry...exiting"
               		quit()
              	else:
			print
			print HOSTUPDATE+" is in Maintenance mode and no vm's running, proceeding upgrade..."

def host_update(HOSTUPDATE,HOSTUPDATEID):
	print
	print HOSTUPDATE +" is updating now....Pls wait..."
	print
	try:
		match=fabric_ssh(HOSTUPDATE)
	except:
		print HOSTUPDATE +" is having issue upgrading the host, please fix it manually... exiting..."
		cancel_maint(HOSTUPDATE,HOSTUPDATEID)
		quit()
	if match == "reboot":
		print match
		cancel_maint(HOSTUPDATE,HOSTUPDATEID)
	elif match == "puppet":
		print match
		print HOSTUPDATE +" is not updated, just ran puppet, it may be already updated, please fix it manually... exiting..."
		cancel_maint(HOSTUPDATE,HOSTUPDATEID)
		quit()
	elif match == "readonly":
		print match
		print HOSTUPDATE +" is having read only filesystem, you may need to re-kickstart the image, please fix it manually... exiting..."
		cancel_maint(HOSTUPDATE,HOSTUPDATEID)   
                quit()
		
def fabric_ssh(HOSTUPDATE):
	env.host_string = HOSTUPDATE
	env.user = SSHUSER
	env.password = SSHPASS
	env.abort_on_prompts=True
	env.warn_only=True
	pattern1 = re.compile(r'Reboot Scheduled')
	pattern2 = re.compile(r'RuntimeError: Previous upgrade completed, you must reboot')
	pattern3 = re.compile(r'Running puppet')
	pattern4 = re.compile(r'OSError: [Errno 30] Read-only file system:')

	def update():
		out1=run('date')
		time.sleep(1)
		out2=sudo('virsh list',shell=False)
		time.sleep(1)
		out3=sudo('/config/update-puppet.sh',shell=False)
		time.sleep(10)
		match1 = pattern1.search(out3)
                match2 = pattern2.search(out3)
                match3 = pattern3.search(out3)
		match4 = pattern4.search(out3)
		if match1 != None or match2 != None:
                        match = "reboot"
                elif match3 != None:
                        match = "puppet"
                elif match4 != None:
                        match = "readonly"
		return match
	match=update()
	return match
	
def check_hostback(HOSTUPDATE,HOSTUPDATEID):
	print
	print HOSTUPDATE +" is Rebooting now, its takes 10 mins to upgrade and reboot... Pls wait..."
	time.sleep(600)

	pingstatus=check_ping(HOSTUPDATE)

	counter=0
	while pingstatus != "Active":
		time.sleep(15)
		counter+=1
		pingstatus=check_ping(HOSTUPDATE)
		if counter > 20:
			print
			print HOSTUPDATE +" is having trouble to come up, please check the status from ilo and bring it up manually, exiting..."
			quit()
	
	if pingstatus == "Active":
		response = csapi.callApi("listHosts",{"id":HOSTUPDATEID})
		for host in response["host"]:
			HOST_STATUS = host["state"]
      	
		counter=0 
        	while HOST_STATUS != "Up":
			print
             		print HOSTUPDATE+" is not yet connected to the cloudstack ...pls wait"
       			counter+=1
       			time.sleep(15)
			response = csapi.callApi("listHosts",{"id":HOSTUPDATEID})
            		for host in response["host"]:
                        	HOST_STATUS = host["state"]
			if counter >= 8:
				print
				print HOSTUPDATE+" is taking long to enter into  Maintenance, May be something wrong on the host... Please check the host manually... exiting..."
                        	quit()	
		
		print
		print HOSTUPDATE+" is up now, time to check updated build version and cancel Maintenance..." 		
	else:
        	print
        	print HOSTUPDATE +" is having trouble to come up, please check the status manually from ilo and fix it, exiting..."
		quit()		

def check_ping(HOSTUPDATE):
    	response = os.system("ping -c 1 " + HOSTUPDATE)
    	if response == 0:
        	pingstatus = "Active"
    	else:
        	pingstatus = "Error"

    	return pingstatus

def check_build(HOSTUPDATE,HOSTUPDATEID):
	print
	print "Checking the current build version after upgrade.."
	try:
       		ssh = paramiko.SSHClient()
        	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        	ssh.connect(HOSTUPDATE, username=SSHUSER, password=SSHPASS)	
		paramiko.util.log_to_file("ssh.log")
	except:
                output = "Auth Err or Host Down, check it manually"
		paramiko.util.log_to_file("ssh.log")

	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("cat /etc/ovirt-node-iso-release")
	out1 = ssh_stdout.read()
	out1 = out1.rstrip()
	print out1
	time.sleep(1)
	
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("uptime")
        out2 = ssh_stdout.read()
        out2 = out2.rstrip()
	print out2
        time.sleep(1)	
	
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("df -h|grep nds|wc -l")
        out3 = ssh_stdout.read()
        out3 = out3.rstrip()
	print out3
        time.sleep(1)	
	
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("free -g|grep Mem|awk \'{print $2}\'")
        out4 = ssh_stdout.read()
        out4 = out4.rstrip()
	print out4
        time.sleep(1)	

	ssh.close()				

#	env.host_string = HOSTUPDATE
#	env.user = SSHUSER
#	env.password = SSHPASS
#	env.abort_on_prompts=True
#	env.warn_only=True

#	def update():
#		out1=run('cat /etc/ovirt-node-iso-release')
#		time.sleep(1)
#		out2=run('uptime')
#		time.sleep(1)
#		out3=run('df -h|grep nds|wc -l')
#		time.sleep(1)
#		out4=run('free -g|grep Mem|awk \'{print $2}\'')
#		time.sleep(1)
#		return out1,out2,out3,out4
#	output=update()

	if out1 == TARGETVER:
		if out3 > 0:
			print
			print HOSTUPDATE +" is upgraded to "+TARGETVER +"and nfs volumes are mounted proceeding ..."
		else:
			print
			print HOSTUPDATE +" is upgraded to "+TARGETVER +"but nfs volumes are not mounted,verify it manually... exiting ..."
			cancel_maint(HOSTUPDATE,HOSTUPDATEID)
                	exit(1) 
       	elif output == "Auth Err or Host Down, check it manually":
		print
		print "couldn't ssh to the host "+HOSTUPDATE +" may or may not be upgraded to "+TARGETVER +" verify it manually... anyway proceeding cancel Maintenance..."
		cancel_maint(HOSTUPDATE,HOSTUPDATEID)
		exit(1) 
	else:
		print
		print HOSTUPDATE +" is not upgraded to "+TARGETVER +" Check the host manually for any noupgrade file under config directory or missing cron entries and then retry if required, proceeding cancel Maintenance..."
		cancel_maint(HOSTUPDATE,HOSTUPDATEID)
		exit(1)

def cancel_maint(HOSTUPDATE,HOSTUPDATEID):
	print
	print HOSTUPDATE +" is coming off maintenance ....Pls wait..."
	try:
		csapi.callApi("cancelHostMaintenance",{"id":HOSTUPDATEID})
	except Exception as e:
		print "error: %s" % e
		print HOSTUPDATE+" is having trouble cancel maintenance, Please check the host manually...cloudstack bug... anyway checking the post status"

def post_state(HOSTUPDATE,HOSTUPDATEID):
	print
	print "Checking the post status....Pls wait..."
	response = csapi.callApi("listHosts",{"id":HOSTUPDATEID})
      	for host in response["host"]:
        	HOST_STATUS = host["state"]
		HOST_RESSTATUS = host["resourcestate"]
	if HOST_STATUS == "Up" and HOST_RESSTATUS == "Enabled":
		print
		print HOSTUPDATE +" looks good, can proceed next host"
	else:
		print
		print HOSTUPDATE +" is having some issues, please check the host manually, exiting..."
		quit() 

def continue_host():
	print
	CONTINUE = raw_input("Do you want to continue update another host? (y/n) ")
	if (CONTINUE == 'y') or (CONTINUE == 'Y') :
		update_host(HOSTDB)
	else:
		print("exiting....")
		quit()

def update_cluster(HOSTDB):
#        print "Development in progress...try the host option for now... exiting"
#        exit(1)
	PREFHOST=""
	PREFHOSTID=""
        for host in HOSTDB:
        	if (HOSTDB[host]['upreq'] == "Yes"):
                	if (HOSTDB[host]['cshoststate'] == "Up"):
                        	if (HOSTDB[host]['csresstate'] == "Enabled"):
                                	if HOSTDB[host]['novms'] > 0:
                                               	HOSTUPDATE=HOSTDB[host]
						HOSTUPDATEID=HOSTDB[host]['hostid']
                                                if len(PREFHOST) == 0:
							list_vms(HOSTUPDATEID)
							prep_maint(HOSTUPDATE,HOSTUPDATEID)
							check_maint(HOSTUPDATE,HOSTUPDATEID)
                                                        host_update(HOSTUPDATE,HOSTUPDATEID)
                                                        check_hostback(HOSTUPDATE,HOSTUPDATEID)
                                                        check_build(HOSTUPDATE,HOSTUPDATEID)
#                                                       cancel_maint(HOSTUPDATE,HOSTUPDATEID)
                                                        post_state(HOSTUPDATE,HOSTUPDATEID)
						else:
                                                       	migrate_vms(HOSTUPDATE,HOSTUPDATEID,PREFHOST,PREFHOSTID)
							prep_maint(HOSTUPDATE,HOSTUPDATEID)
							check_maint(HOSTUPDATE,HOSTUPDATEID)
							host_update(HOSTUPDATE,HOSTUPDATEID)
							check_hostback(HOSTUPDATE,HOSTUPDATEID)
							check_build(HOSTUPDATE,HOSTUPDATEID)
#							cancel_maint(HOSTUPDATE,HOSTUPDATEID)
							post_state(HOSTUPDATE,HOSTUPDATEID)
        					PREFHOST=HOSTUPDATE
                                                PREFHOSTID=HOSTDB[PREFHOST]['hostid']
						time.sleep(30)
						print
						print "Continuing next host..."
						continue
	                            	else:
						print
                                                print "No Running VM's on this host...proceeding..."
                                                HOSTUPDATE=HOSTDB[host]
                                                HOSTUPDATEID=HOSTDB[host]['hostid']
						prep_maint(HOSTUPDATE,HOSTUPDATEID)
						check_maint(HOSTUPDATE,HOSTUPDATEID)
                                                host_update(HOSTUPDATE,HOSTUPDATEID)
                                                check_hostback(HOSTUPDATE,HOSTUPDATEID)
                                                check_build(HOSTUPDATE,HOSTUPDATEID)
#                                               cancel_maint(HOSTUPDATE,HOSTUPDATEID)
                                                post_state(HOSTUPDATE,HOSTUPDATEID)
					PREFHOST=HOSTUPDATE
                                        PREFHOSTID=HOSTDB[PREFHOST]['hostid']
					time.sleep(30)
                                        print 
                                        print "Continuing next host..."
					continue
                               	elif (HOSTDB[host]['csresstate'] == "PrepareForMaintenance"):
                                        print
                                        print "Host is in Preparing for Maintenance... continuing"
                                        check_maint(HOSTUPDATE,HOSTUPDATEID)
					host_update(HOSTUPDATE,HOSTUPDATEID)
                                       	check_hostback(HOSTUPDATE,HOSTUPDATEID)
                                        check_build(HOSTUPDATE,HOSTUPDATEID)
#                                       cancel_maint(HOSTUPDATE,HOSTUPDATEID)
                                        post_state(HOSTUPDATE,HOSTUPDATEID)
					PREFHOST=HOSTUPDATE
					PREFHOSTID=HOSTDB[PREFHOST]['hostid']
					time.sleep(30)
                                       	print 
                                      	print "Continuing next host..."
					continue
                                elif (HOSTDB[host]['csresstate'] == "Maintenance"):
                                        print
                                        print "Host is already in Maintenance state, proceeding upgrade"
                                        host_update(HOSTUPDATE,HOSTUPDATEID)
                                	check_hostback(HOSTUPDATE,HOSTUPDATEID)
                                	check_build(HOSTUPDATE,HOSTUPDATEID)
#                                	cancel_maint(HOSTUPDATE,HOSTUPDATEID)
                                	post_state(HOSTUPDATE,HOSTUPDATEID)
                                        PREFHOST=HOSTUPDATE
                                        PREFHOSTID=HOSTDB[PREFHOST]['hostid']
					time.sleep(30)
                                       	print 
                                       	print "Continuing next host..."
					continue
                                elif (HOSTDB[host]['csresstate'] == "ErrorInMaintenance"):
                                        print
                                        print "Host is in Error State, Can't proceed upgrade... Continuing next host..."
                                	continue
                     	elif (HOSTDB[host]['cshoststate'] == "Alert" or HOSTDB[host]['cshoststate'] == "Down" or HOSTDB[host]['cshoststate'] == "Disconnected" or HOSTDB[host]['cshoststate'] == "Connecting"):
                             	print
                                print "Host is in ALERT/Down/Disconnected/Connecting State, can't proceed update, please fix the host manually and retry... Continuing next host..."
				continue
             	elif (HOSTDB[host]['upreq'] == "No"):
                       	print HOSTUPDATE+" is already running required verison, no update required... Continuing next host..."
			continue
		else:
			continue
	
def main():
	list_zones()

	print
	print

	try:
		ZONEID = raw_input("Enter the zone id to list POD's: ")
	except(KeyboardInterrupt):
		print
		print("Oops! Keyboard Interrupt... exiting...")
		exit(1) 

	list_pods(ZONEID)

	print
	print

	try:
		PODID = raw_input("Enter the POD id to list Cluster's: ")
	except(KeyboardInterrupt):
        	print
        	print("Oops! Keyboard Interrupt... exiting...")
        	exit(1)

	list_clusters(PODID)

	print
	print

	try:
		CLUSTERID = raw_input("Enter the KVM Cluster id to update HOST's: ")
	except(KeyboardInterrupt):
        	print
        	print("Oops! Keyboard Interrupt... exiting...")
        	exit(1)

	find_hosts(CLUSTERID)
	find_novms(HOSTSID)
	find_sshable(HOSTS)
	find_build(HOSTS)

	print
	print
	print ("Collecting host details.... pls wait.....")

	list_hostdb(CLUSTERID,HOSTS,HOSTSID,BUILD,NOVMS,SSHABLE,CSHOSTSTATE,CSRESSTATE,UPREQ)

	print
	print
	CAPAVAIL = raw_input("Do you have enough capacity to upgrade a host or entire cluster? (y/n) ")
	if (CAPAVAIL == 'y') or (CAPAVAIL == 'Y') :
		print
		HOSTORCLUS = raw_input("Do you want to upgrade a host or entire cluster? (h/c) ")
		if (HOSTORCLUS == 'h') or (HOSTORCLUS == 'H'):
			update_host(HOSTDB)
			continue_host()
		elif (HOSTORCLUS == 'c') or (HOSTORCLUS == 'C'):
			update_cluster(HOSTDB)
			print
        		print "All the hosts are updated to required build, Exiting"
		else:
			print
			print("Sorry. That's not a valid input... exiting....")
	elif (CAPAVAIL == 'n') or (CAPAVAIL == 'N'):
		print
		print("Ok... Please migrate some vm's to another POD to create capacity or add hosts to this cluster and retry... exiting....")
        	quit()
	else:
		print
		print("Sorry. That's not a valid input... exiting....")

if __name__ == "__main__":
	main()