#!/bin/bash
LOG_DIR_BASE=/ngs1/app
for i in `more /tmp/users_uat.yml `
do
    echo $i
    ROGRP=`echo $i | sed 's/edwae/edwro/g'`
    cp /ngs/app/$i/.profile /ngs/app/$i/.profile_backup_$(date +%Y-%m-%d-%H.%M.%S)
    cp /tmp/profile /ngs/app/$i/.profile
    chown $i /ngs/app/$i/.profile
    ls -ltr /ngs/app/$i/.profile
    mkdir $LOG_DIR_BASE/$i/log
    chmod 2750 $LOG_DIR_BASE/$i/log
    chown $i:$ROGRP $LOG_DIR_BASE/$i/log
    mkdir $LOG_DIR_BASE/$i/log/ClusterMaintenance
    mkdir $LOG_DIR_BASE/$i/log/CompactionEliminator
    mkdir $LOG_DIR_BASE/$i/log/HiveCompaction
    mkdir $LOG_DIR_BASE/$i/log/Compaction
    mkdir $LOG_DIR_BASE/$i/log/DataMigration
    mkdir $LOG_DIR_BASE/$i/log/ETLListener
    mkdir $LOG_DIR_BASE/$i/log/ETLListener
    mkdir $LOG_DIR_BASE/$i/log/FileToCore
    mkdir $LOG_DIR_BASE/$i/log/ObfuscateCopy
    mkdir $LOG_DIR_BASE/$i/log/DataReplication
    mkdir $LOG_DIR_BASE/$i/log/SemanticFwrkIP
    mkdir $LOG_DIR_BASE/$i/log/SemanticFwrk
    mkdir $LOG_DIR_BASE/$i/log/SqoopTool
    mkdir $LOG_DIR_BASE/$i/log/Autosys
    chmod 2750 $LOG_DIR_BASE/$i/log/*
    chown $i:$ROGRP $LOG_DIR_BASE/$i/log/*
    ln -s $LOG_DIR_BASE/$i/log /ngs/app/$i/logs
    chown -h  $i:$ROGRP /ngs/app/$i/logs
    ls -ltr $LOG_DIR_BASE/$i/log
done